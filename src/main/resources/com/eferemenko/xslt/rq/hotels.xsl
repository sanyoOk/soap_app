<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://eferemenko.com/entities"
                exclude-result-prefixes="ent">

    <xsl:output method="text"/>

    <xsl:template match="/">
        <xsl:variable name="countryCode" select="ent:HotelsRequest/ent:CodeCountry"/>
        <xsl:variable name="cityName" select="ent:HotelsRequest/ent:CityName"/>
        <xsl:variable name="language" select="ent:HotelsRequest/ent:Language"/>
        <xsl:variable name="hotelName" select="ent:HotelsRequest/ent:HotelName"/>
        <xsl:variable name="hotelCategory" select="ent:HotelsRequest/ent:HotelCategory"/>

        <xsl:text>
            select
                gln1.name as country,
                gl1.code as country_code,
                gln.name as city,
                gl.code as city_code,
                ghn.name as hotel,
                gh.code as hotel_code,
                gh.phone as phone,
                gh.fax as fax,
                gh.email as email,
                gh.url as url,
                gh.category_id as category_id,
                gh.check_in as check_in,
                gh.check_out as check_out,
                gh.longitude as longitude,
                gh.latitude as latitude,
                ghi.url as picture_url,
                ghit.type_name as type_picture
            from gpt_hotel gh
                inner join gpt_location gl on gh.city_id = gl.id
                inner join gpt_location_name gln on gl.id = gln.location_id
                inner join gpt_hotel_name ghn on gh.id = ghn.hotel_id
                inner join gpt_location_name gln1 on gh.country_id = gln1.location_id
                inner join gpt_location gl1 on gh.country_id = gl1.id
                inner join gpt_hotel_images ghi on gh.id = ghi.hotel_id
                inner join gpt_hotel_image_type ghit on ghi.type_id = ghit.id
            where
                gh.country_id in (select id from gpt_location where type_id = 3 and code = "</xsl:text>
        <xsl:value-of select="$countryCode"/>
        <xsl:text>")</xsl:text>
        <xsl:if test="string-length($language) &gt; 0">
            <xsl:text>
                and gln.lang_id in (select id from gpt_language where code = "</xsl:text>
            <xsl:value-of select="$language"/>
            <xsl:text>")
                and gln1.lang_id in (select id from gpt_language where code = "</xsl:text>
            <xsl:value-of select="$language"/>
            <xsl:text>")
                and ghn.lang_id in (select id from gpt_language where code = "</xsl:text>
            <xsl:value-of select="$language"/>
            <xsl:text>")</xsl:text>
        </xsl:if>
        <xsl:if test="string-length($cityName) &gt; 0">
            <xsl:text>
                and gln.name = "</xsl:text>
            <xsl:value-of select="$cityName"/>
            <xsl:text>"</xsl:text>
        </xsl:if>
        <xsl:if test="string-length($hotelName) &gt; 0">
            <xsl:text>
                and ghn.name like "%</xsl:text>
            <xsl:value-of select="$hotelName"/>
            <xsl:text>%"</xsl:text>
        </xsl:if>
        <xsl:if test="string-length($hotelCategory) &gt; 0">
            <xsl:text>
                and gh.category_id = "</xsl:text>
            <xsl:value-of select="$hotelCategory"/>
            <xsl:text>"</xsl:text>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>

