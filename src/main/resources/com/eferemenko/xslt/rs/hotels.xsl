<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://eferemenko.com/entities"
                exclude-result-prefixes="ent">

    <xsl:output method="xml"/>

    <xsl:template match="/">

        <ent:HotelsResponse>
            <xsl:for-each select="Result/Entry">
                <ent:Country Name="{country}" Code="{country_code}">
                    <ent:City Name="{city}" Code="{city_code}">
                        <ent:Hotel Name="{hotel}" Code="{hotel_code}" HotelCategory="{category_id}">
                            <ent:Phone>
                                <xsl:value-of select="phone"></xsl:value-of>
                            </ent:Phone>
                            <ent:Fax>
                                <xsl:value-of select="fax"></xsl:value-of>
                            </ent:Fax>
                            <ent:Email>
                                <xsl:value-of select="email"></xsl:value-of>
                            </ent:Email>
                            <ent:UrlAddress>
                                <xsl:value-of select="url"></xsl:value-of>
                            </ent:UrlAddress>
                            <ent:Check_in>
                                <xsl:value-of select="check_in"></xsl:value-of>
                            </ent:Check_in>
                            <ent:Check_out>
                                <xsl:value-of select="check_out"></xsl:value-of>
                            </ent:Check_out>
                            <ent:Latitude>
                                <xsl:value-of select="latitude"></xsl:value-of>
                            </ent:Latitude>
                            <ent:Longitude>
                                <xsl:value-of select="longitude"></xsl:value-of>
                            </ent:Longitude>
                            <ent:Pictures>
                                <ent:Picture>
                                    <ent:Url>
                                        <xsl:value-of select="picture_url"></xsl:value-of>
                                    </ent:Url>
                                    <ent:ImageType>
                                        <xsl:value-of select="type_picture"></xsl:value-of>
                                    </ent:ImageType>
                                </ent:Picture>
                            </ent:Pictures>
                        </ent:Hotel>
                    </ent:City>
                </ent:Country>
            </xsl:for-each>
        </ent:HotelsResponse>
    </xsl:template>

</xsl:stylesheet>