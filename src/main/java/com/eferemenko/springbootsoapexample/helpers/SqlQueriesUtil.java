package com.eferemenko.springbootsoapexample.helpers;

import com.eferemenko.springbootsoapexample.dao.XmlAbstractDAO;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class SqlQueriesUtil {
	
	private static XmlAbstractDAO xmlAbstractDAO;
	
	public static NodeList subQuery(final String query) {
		final Document doc = xmlAbstractDAO.selectDataFromDB(query);
		
		return doc.getChildNodes();
	}
	
	public void setXmlAbstractDAO(final XmlAbstractDAO xmlAbstractDAO) {
		SqlQueriesUtil.xmlAbstractDAO = xmlAbstractDAO;
	}
	
}
