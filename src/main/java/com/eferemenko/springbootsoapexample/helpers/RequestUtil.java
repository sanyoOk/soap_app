package com.eferemenko.springbootsoapexample.helpers;

import com.eferemenko.springbootsoapexample.processors.Session;

public class RequestUtil {
	
	public static void overrideResponse() {
		final Session session = Session.getSession();
		session.setRequestOk(false);
	}
	
}
