package com.eferemenko.springbootsoapexample.filters;

import com.eferemenko.springbootsoapexample.processors.ServiceException;

public interface PreFilter <RQ> {
	
	public boolean isApplicable(RQ request);
	
	public void preProcess(RQ request) throws ServiceException;
	
}
