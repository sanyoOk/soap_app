package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.entities.LanguageType;
import com.eferemenko.springbootsoapexample.dao.HotelsDao;
import com.eferemenko.springbootsoapexample.filters.PreFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

import java.util.Arrays;

public class LanguageFilter implements PreFilter<HotelsRequest> {

    @Override
    public boolean isApplicable(HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        LanguageType language = request.getLanguage();
        if (language == null) {
            throw new  ServiceException("incorrect language (ru/en only)");
        }
    }

}
