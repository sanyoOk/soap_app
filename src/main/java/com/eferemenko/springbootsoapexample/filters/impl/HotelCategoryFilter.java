package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.springbootsoapexample.dao.HotelsDao;
import com.eferemenko.springbootsoapexample.filters.PreFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

public class HotelCategoryFilter implements PreFilter<HotelsRequest> {

    @Override
    public boolean isApplicable(HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        String category = request.getHotelCategory();
        if (category != null && !category.isEmpty()) {
            switch (category) {
                case "1" :
                    break;
                case "2" :
                    break;
                case "3" :
                    break;
                case "4" :
                    break;
                case "5" :
                    break;
                default:
                    throw new ServiceException("The hotel category = " + category + " is not issue");
            }
        }
    }
}
