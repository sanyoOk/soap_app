package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.CountryRequest;
import com.eferemenko.springbootsoapexample.dao.CountriesDAO;
import com.eferemenko.springbootsoapexample.filters.PreFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

import java.util.List;

public class CheckCountryFilter implements PreFilter<CountryRequest> {
	
	private CountriesDAO countriesDAO;
	
	@Override
	public boolean isApplicable(final CountryRequest request) {
		return true;
	}

	@Override
	public void preProcess(final CountryRequest request) throws ServiceException {
		if (request.getName() != null) {
			final List<String> countryNames = countriesDAO.getCountriesNames();
			if (!countryNames.contains(request.getName())) {
				throw new ServiceException("Requested country name is incorrect");
			}
		}
	}
	
	public void setCountriesDAO(CountriesDAO countriesDAO) {
		this.countriesDAO = countriesDAO;
	}
	
}
