package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.CityType;
import com.eferemenko.entities.Country;
import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.entities.HotelsResponse;
import com.eferemenko.springbootsoapexample.filters.PostFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GroupCountriesFilter implements PostFilter<HotelsRequest, HotelsResponse> {
    @Override
    public boolean isApplicable(HotelsRequest request, HotelsResponse response) {
        return !response.getCountry().isEmpty();
    }

    @Override
    public void postProcess(HotelsRequest request, HotelsResponse response) throws ServiceException {
        final List<Country> countriesDelete = new ArrayList<>();
        final Map<String, List<CityType>> citiesMap = new HashMap<>();

        for (Country country: response.getCountry()) {
            String countryName = country.getName();
            if (citiesMap.containsKey(countryName)) {
                countriesDelete.add(country);
            }

            if (country.getCity() == null) {
                continue;
            }

            List<CityType> cities = citiesMap.get(countryName);
            if (cities == null) {
                cities = new ArrayList<>();
                citiesMap.put(countryName, cities);
            }
            cities.addAll(country.getCity());
        }

        response.getCountry().removeAll(countriesDelete);

        for (Country country : response.getCountry()) {
            country.getCity().clear();

            final List<CityType> citiesList = citiesMap.get(country.getName());
            if (citiesList != null) {
                country.getCity().addAll(citiesList);
            }
        }
    }
}
