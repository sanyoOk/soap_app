package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.springbootsoapexample.dao.HotelsDao;
import com.eferemenko.springbootsoapexample.filters.PreFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

import java.util.List;

public class CheckCountryCodeFilter implements PreFilter<HotelsRequest> {
    private HotelsDao hotelsDao;

    @Override
    public boolean isApplicable(HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        if (request.getCodeCountry() != null && !request.getCodeCountry().isEmpty()) {
            final List<String> countryCodes = hotelsDao.getCountriesCode();
            if (!countryCodes.contains(request.getCodeCountry())) {
                throw new ServiceException("Requested country code is incorrect");
            }
        } else if (request.getCodeCountry() == null){
            throw new ServiceException("Requested country code is null");
        } else if (request.getCodeCountry().isEmpty()){
            throw new ServiceException("Requested country code is empty");
        }
    }

    public void setHotelsDao(HotelsDao hotelsDao) {
        this.hotelsDao = hotelsDao;
    }
}
