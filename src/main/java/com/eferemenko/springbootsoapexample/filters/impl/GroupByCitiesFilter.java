package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.CityType;
import com.eferemenko.entities.Hotel;
import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.entities.HotelsResponse;
import com.eferemenko.springbootsoapexample.filters.PostFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupByCitiesFilter implements PostFilter<HotelsRequest, HotelsResponse> {
    @Override
    public boolean isApplicable(HotelsRequest request, HotelsResponse response) {
        return !response.getCountry().isEmpty();
    }

    @Override
    public void postProcess(HotelsRequest request, HotelsResponse response) throws ServiceException {
        final List<CityType> citiesDelete = new ArrayList<>();
        final Map<String, List<Hotel>> citiesMap = new HashMap<>();

        for (CityType city: response.getCountry().get(0).getCity()) {
            String cityCode = city.getCode();
            if (citiesMap.containsKey(cityCode)) {
                citiesDelete.add(city);
            }

            if (city.getHotel() == null) {
                continue;
            }

            List<Hotel> hotels = citiesMap.get(cityCode);
            if (hotels == null) {
                hotels = new ArrayList<>();
                citiesMap.put(cityCode, hotels);
            }
            hotels.addAll(city.getHotel());
        }

        response.getCountry().get(0).getCity().removeAll(citiesDelete);

        for (CityType city: response.getCountry().get(0).getCity()) {
            city.getHotel().clear();

            final List<Hotel> hotelsList = citiesMap.get(city.getCode());
            if (hotelsList != null) {
                city.getHotel().addAll(hotelsList);
            }
        }
    }
}
