package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.springbootsoapexample.dao.HotelsDao;
import com.eferemenko.springbootsoapexample.filters.PreFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

import java.util.List;

public class CheckCityByCountryCodeFilter implements PreFilter<HotelsRequest> {
    private HotelsDao hotelsDao;

    @Override
    public boolean isApplicable(HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        if (request.getCityName() != null && !request.getCityName().isEmpty()) {
            final List<String> cities = hotelsDao.getCitiesByCountryCode(request.getCodeCountry(), request.getLanguage().value());
            if (!cities.contains(request.getCityName())) {
                throw new ServiceException("Requested city does not issue in the country with code=" + request.getCodeCountry());
            }
        }
    }

    public void setHotelsDao(HotelsDao hotelsDao) {
        this.hotelsDao = hotelsDao;
    }
}
