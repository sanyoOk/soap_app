package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.CityType;
import com.eferemenko.entities.Hotel;
import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.entities.HotelsResponse;
import com.eferemenko.entities.Picture;
import com.eferemenko.springbootsoapexample.filters.PostFilter;
import com.eferemenko.springbootsoapexample.processors.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupPictures implements PostFilter<HotelsRequest, HotelsResponse> {
    @Override
    public boolean isApplicable(HotelsRequest request, HotelsResponse response) {
        return !response.getCountry().isEmpty();
    }

    @Override
    public void postProcess(HotelsRequest request, HotelsResponse response) throws ServiceException {
        final List<Hotel> hotelsDelete = new ArrayList<>();
        final Map<String, List<Picture>> hotelsMap = new HashMap<>();

        for (CityType city: response.getCountry().get(0).getCity()) {
            for (Hotel hotel: city.getHotel()) {
                String hotelCode = hotel.getCode();
                if (hotelsMap.containsKey(hotelCode)) {
                    hotelsDelete.add(hotel);
                }

                if (hotel.getPictures().getPicture() == null) {
                    continue;
                }

                List<Picture> pictures = hotelsMap.get(hotelCode);
                if (pictures == null) {
                    pictures = new ArrayList<>();
                    hotelsMap.put(hotelCode, pictures);
                }
                pictures.addAll(hotel.getPictures().getPicture());
            }

            city.getHotel().removeAll(hotelsDelete);

            for (Hotel hotel: city.getHotel()) {
                hotel.getPictures().getPicture().clear();

                final List<Picture> pictureList = hotelsMap.get(hotel.getCode());
                if (pictureList != null) {
                    hotel.getPictures().getPicture().addAll(pictureList);
                }
            }
        }
    }
}
