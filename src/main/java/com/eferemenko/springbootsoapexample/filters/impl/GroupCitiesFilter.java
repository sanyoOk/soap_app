package com.eferemenko.springbootsoapexample.filters.impl;

import com.eferemenko.entities.CitiesType;
import com.eferemenko.entities.CountryRequest;
import com.eferemenko.entities.CountryResponse;
import com.eferemenko.entities.CountryType;
import com.eferemenko.springbootsoapexample.filters.PostFilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GroupCitiesFilter implements PostFilter<CountryRequest, CountryResponse> {

	@Override
	public boolean isApplicable(final CountryRequest request, final CountryResponse response) {
		if (response.getCountry().isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public void postProcess(final CountryRequest request, final CountryResponse response) {
		final List<CountryType> countriesDeleteList = new ArrayList<>();
		final Map<String, List<CitiesType.City>> citiesMap = new HashMap<>();
		for (final CountryType country : response.getCountry()) {
			final String countryName = country.getName();
			if (citiesMap.containsKey(countryName)) {
				countriesDeleteList.add(country);
			}
			
			if (country.getCities() == null) {
				continue;
			}
			
			List<CitiesType.City> cities = citiesMap.get(countryName);
			if (cities == null) {
				cities = new ArrayList<>();
				citiesMap.put(countryName, cities);
			}
			cities.addAll(country.getCities().getCity());
		}
		
		response.getCountry().removeAll(countriesDeleteList);
		
		final Iterator<CountryType> countryIterator = response.getCountry().iterator();
		while (countryIterator.hasNext()) {
			final CountryType country = countryIterator.next();
			final CitiesType cities = country.getCities();
			if (cities == null) {
				continue;
			}
			
			cities.getCity().clear();
			
			final List<CitiesType.City> citiesList = citiesMap.get(country.getName());
			if (citiesList != null) {
				cities.getCity().addAll(citiesList);
			}
		}
	}
	
}
