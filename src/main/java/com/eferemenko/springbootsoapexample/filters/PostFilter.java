package com.eferemenko.springbootsoapexample.filters;


import com.eferemenko.springbootsoapexample.processors.ServiceException;

public interface PostFilter <RQ, RS> {
	
	public boolean isApplicable(RQ request, RS response);
	
	public void postProcess(RQ request, RS response) throws ServiceException;
	
}
