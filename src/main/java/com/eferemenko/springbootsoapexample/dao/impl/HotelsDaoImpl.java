package com.eferemenko.springbootsoapexample.dao.impl;

import com.eferemenko.springbootsoapexample.dao.HotelsDao;
import com.eferemenko.springbootsoapexample.dao.JdbcExt;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class HotelsDaoImpl extends JdbcExt implements HotelsDao {

    private static final String COUNTRIES_BY_CODE = "SELECT CODE FROM GPT_LOCATION WHERE TYPE_ID = 3 GROUP BY CODE";
    private static final String CITIES_BY_COUNTRY_CODE = "select gln.name name from gpt_location l inner join gpt_location_name gln on l.id=gln.location_id" +
            " where l.type_id = 4 and l.parent in(select id from gpt_location where type_id = 3 and code = \"";
    private static final String LANGUAGES = "select code from gpt_language";

    @Override
    public List<String> getCountriesCode() {
        return getJdbcTemplate().query(COUNTRIES_BY_CODE, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getString("code");
            }
        });
    }

    @Override
    public List<String> getCitiesByCountryCode(String code,String language) {
        String sqlQuery = CITIES_BY_COUNTRY_CODE + code + "\") and gln.lang_id in (select id from gpt_language where code = \"" + language + "\")";
        return getJdbcTemplate().query(sqlQuery, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getString("name");
            }
        });
    }

    @Override
    public List<String> getLanguages() {
        return getJdbcTemplate().query(LANGUAGES, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getString("code");
            }
        });
    }
}
