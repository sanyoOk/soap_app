package com.eferemenko.springbootsoapexample.dao;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface HotelsDao {
    @Transactional(
            readOnly = true,
            rollbackFor = Throwable.class
    )
    List<String> getCountriesCode();

    @Transactional(
            readOnly = true,
            rollbackFor = Throwable.class
    )
    List<String> getCitiesByCountryCode(String code, String language);

    @Transactional(
            readOnly = true,
            rollbackFor = Throwable.class
    )
    List<String> getLanguages();
}
