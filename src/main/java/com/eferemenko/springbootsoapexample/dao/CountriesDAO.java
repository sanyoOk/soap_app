package com.eferemenko.springbootsoapexample.dao;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CountriesDAO {
	
	@Transactional(
		readOnly = true, 
		rollbackFor = Throwable.class
	)
	List<String> getCountriesNames();
	
}
