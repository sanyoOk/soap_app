package com.eferemenko.springbootsoapexample.processors.impl;

import com.eferemenko.entities.CountryRequest;
import com.eferemenko.entities.CountryResponse;
import com.eferemenko.springbootsoapexample.processors.AbstractServiceProcessor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class CountriesProcessor extends AbstractServiceProcessor<CountryRequest, CountryResponse> {

	@Override
	protected Marshaller createMarshaller() throws JAXBException {
		return JAXBContext.newInstance(CountryRequest.class).createMarshaller();
	}
	
	@Override
	protected Unmarshaller createUnmarshaller() throws JAXBException {
		return JAXBContext.newInstance(CountryResponse.class).createUnmarshaller();
	}
	
	@Override
	protected CountryResponse castResult(final Object response) {
		return (CountryResponse) response;
	}

}
