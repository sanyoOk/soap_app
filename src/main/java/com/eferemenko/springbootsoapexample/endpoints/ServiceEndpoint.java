package com.eferemenko.springbootsoapexample.endpoints;

import com.eferemenko.entities.CountryRequest;
import com.eferemenko.entities.CountryResponse;
import com.eferemenko.entities.FaultMessage;
import com.eferemenko.entities.HotelsRequest;
import com.eferemenko.entities.HotelsResponse;
import com.eferemenko.entities.ServicePort;
import com.eferemenko.springbootsoapexample.processors.ServiceException;
import com.eferemenko.springbootsoapexample.processors.impl.CountriesProcessor;
import com.eferemenko.springbootsoapexample.processors.impl.HotelsProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

@Endpoint
public class ServiceEndpoint implements ServicePort {
	
	private static final String NAMESPACE_URI = "http://eferemenko.com/entities";
	
	@Autowired
	private CountriesProcessor countriesProcessor;
	
	@Autowired
	private HotelsProcessor hotelsProcessor;
	
	@Override
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "CountryRequest")
	@ResponsePayload
	public CountryResponse processCountryRequest(@RequestPayload CountryRequest countryRequest) throws FaultMessage {
		CountryResponse response = null;
		try {
			response = countriesProcessor.process(countryRequest);
		} catch (JAXBException | IOException | TransformerException e) {
			throw new FaultMessage("Unknown error: " + e.getMessage());
		} catch (ServiceException e) {
			throw new FaultMessage("Service error: " + e.getMessage());
		}
		
		return response;
	}

	@Override
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "HotelsRequest")
	@ResponsePayload
	public HotelsResponse processHotelsRequest(@RequestPayload HotelsRequest hotelsRequest) throws FaultMessage {
		HotelsResponse response = null;
		try {
			response = hotelsProcessor.process(hotelsRequest);
		} catch (JAXBException | IOException | TransformerException e) {
			throw new FaultMessage("Unknown error: " + e.getMessage());
		} catch (ServiceException e) {
			throw new FaultMessage("Service error: " + e.getMessage());
		}
		
		return response;
	}
}
